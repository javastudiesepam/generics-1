package com.epam.training.generics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

class CircularBufferTest {
  private static final String ANSI_RESET = "\u001B[0m";
  private static final String ANSI_RED = "\u001B[31m";
  private static final String ANSI_GREEN = "\u001B[32m";

  static void test() {
    System.out.println("CircularBuffer test\n");

    testPut();
    testGet();
    testIsEmpty();
    testToObjArr();
    testToArray();
    asList();
    addAll();
    compare();
  }

  private static void compare() {
    String it = "Should sort circular buffer content using the passed comparator";

    CircularBuffer<TestWithValue> circularBuffer = new CircularBuffer<>(TestWithValue.class,4);
    Comparator<TestWithValue> testWithNameComparator = new TestWithNameComparator();

    TestWithValue test1 = new TestWithValue(25);
    TestWithValue test2 = new TestWithValue(1);
    TestWithValue test3 = new TestWithValue(42);
    TestWithValue[] expectedResult = {test2, test1, test3};

    circularBuffer.put(test1);
    circularBuffer.put(test2);
    circularBuffer.put(test3);
    circularBuffer.get();
    circularBuffer.get();
    circularBuffer.put(test1);
    circularBuffer.put(test2);
    circularBuffer.sort(testWithNameComparator);

    TestWithValue[] result = circularBuffer.toArray();

    boolean assumption = Arrays.equals(result, expectedResult);
    printResult(assumption, it);
  }

  private static void addAll() {
    Test test1 = new Test();
    Test test2 = new Test();
    boolean assumption = false;

    {
      String it = "addAll should throw an error if no space in circularBuffer";

      CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class,2);
      List<Test> parentList = new ArrayList<>();

      parentList.add(test1);
      parentList.add(test2);
      circularBuffer.put(new Test());

      try {
        circularBuffer.addAll(parentList);
      } catch (RuntimeException e) {
        assumption = true;
      }

      printResult(assumption, it);
    }

    {
      String it = "addAll should add a list of items if there is enough space in circularBuffer";

      CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class,2);
      List<Test> parentList = new ArrayList<>();

      parentList.add(test1);
      parentList.add(test2);
      circularBuffer.addAll(parentList);

      Test[] result = circularBuffer.toArray();
      Test[] expectedResult = {test1, test2};

      assumption = Arrays.equals(result, expectedResult);
      printResult(assumption, it);
    }

    {
      String it = "addAll should accept generic's type children";

      CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class,2);
      List<Test> parentList = new ArrayList<>();
      TestChild child1 = new TestChild();
      TestChild child2 = new TestChild();

      parentList.add(child1);
      parentList.add(child2);
      try {
        circularBuffer.addAll(parentList);
        assumption = true;
      } catch (RuntimeException e) {
        assumption = false;
      }

      printResult(assumption, it);
    }
  }

  private static void asList() {
    CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class,2);
    Test test1 = new Test();
    Test test2 = new Test();

    String it = "asList should return List";

    circularBuffer.put(test1);
    circularBuffer.put(test2);
    List<Test> result = circularBuffer.asList();
    boolean assumption = result != null;
    printResult(assumption, it);
  }

  private static void testToArray() {
    CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class, 2);
    Test test1 = new Test();
    Test test2 = new Test();
    Object[] expectedResult = {test1, test2};

    String it = "toObjectArray should return an array of Object in the right order";

    circularBuffer.put(test1);
    circularBuffer.put(test2);
    Test[] result = circularBuffer.toArray();

    boolean assumption = Arrays.equals(result, expectedResult);

    printResult(assumption, it);
  }

  private static void testToObjArr() {
    Test test1 = new Test();
    Test test2 = new Test();
    Object[] expectedResult = {test1, test2};

    {
      String it = "toObjectArray should return an array of Object in the right order";

      CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class, 2);
      circularBuffer.put(test1);
      circularBuffer.put(test2);
      Object[] result = circularBuffer.toObjectArray();

      boolean assumption = Arrays.equals(result, expectedResult);

      printResult(assumption, it);
    }

    {
      String it = "toObjectArray should return an array of Object in the right order after a few operations";

      CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class, 2);
      circularBuffer.put(new Test());
      circularBuffer.put(test1);
      circularBuffer.get();
      circularBuffer.put(test2);
      Object[] result = circularBuffer.toObjectArray();

      boolean assumption = Arrays.equals(result, expectedResult);
      printResult(assumption, it);
    }
  }

  private static void testGet() {
    CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class, 2);

    String it = "Get on empty buffer should throw an error";

    boolean assumption = false;

    try {
      circularBuffer.get();
    } catch (RuntimeException e) {
      assumption = true;
    }
    printResult(assumption, it);

    it = "Get on NOT empty buffer should return a Test object";
    circularBuffer.put(new Test());
    Test test = circularBuffer.get();
    assumption = test != null;
    printResult(assumption, it);
  }

  private static void testPut() {
    boolean assumption;
    {
      String it = "Should add an object to circular buffer: ";
      CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class, 2);

      Test test = new Test();
      circularBuffer.put(test);

      Test result = circularBuffer.get();
      assumption = test == result;
      printResult(assumption, it);
    }
    {
      String it = "Should throw a runtime exception when buffer is full: ";

      CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class, 2);
      circularBuffer.put(new Test());
      circularBuffer.put(new Test());
      try {
        circularBuffer.put(new Test());
      } catch (RuntimeException e) {
        assumption = true;
      }
      printResult(assumption, it);
    }

    {
      String it = "Should not throw an exception if elements were read from the buffer: ";
      assumption = true;

      CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class, 2);
      circularBuffer.put(new Test());
      circularBuffer.put(new Test());
      circularBuffer.get();
      try {
        circularBuffer.put(new Test());
      } catch (RuntimeException e) {
        assumption = false;
      }

      printResult(assumption, it);
    }
  }

  private static void testIsEmpty() {
    boolean assumption;
    {
      String it = "isEmpty true when no elements were added";

      CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class, 2);
      assumption = circularBuffer.isEmpty();

      printResult(assumption, it);
    }
    {
      String it = "isEmpty false after an element was added";

      CircularBuffer<Test> circularBuffer = new CircularBuffer<>(Test.class, 2);
      circularBuffer.put(new Test());
      assumption = !circularBuffer.isEmpty();
      printResult(assumption, it);
    }
  }

  private static void printResult(Boolean assumption, String it) {
    if (assumption) {
      System.out.println(ANSI_GREEN + "pass " + ANSI_RESET + it);
    } else {
      System.out.println(ANSI_RED + "fail " + ANSI_RESET + it);
    }
  }
}

class Test {}

class TestWithValue {
  private int value;

  TestWithValue(int value) {
    this.value = value;
  }

  int getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "TestWithValues " + value;
  }
}

class TestWithNameComparator implements Comparator<TestWithValue> {
  @Override
  public int compare(TestWithValue o1, TestWithValue o2) {
    return o1.getValue() - o2.getValue();
  }
}

class TestChild extends Test {}