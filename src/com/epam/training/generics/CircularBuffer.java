package com.epam.training.generics;

import java.lang.reflect.Array;
import java.util.*;

class CircularBuffer<T> {
  public T[] elements;

  public int capacity;
  public int writePos = 0;
  public int readPos = 0;
  public boolean flipped = false;

  @SuppressWarnings("unchecked")
  public CircularBuffer(Class<T> typeClass, int capacity) {
    this.capacity = capacity;
    this.elements = (T[]) Array.newInstance(typeClass, capacity);
  }

  void sort(Comparator<? super T> comparator) {
    T[] elements = toArray();
    Arrays.sort(elements, comparator);


    if (!flipped) {
    //  in this case we put back elements starting from read position
      int iterationCounter = 0;
      for (int i = readPos; i < elements.length; i++) {
        this.elements[i] = elements[iterationCounter++];
      }
    } else {
    //  in this case we put back elements at the end and at the start

      //writing to top
      int iterationCounter = 0;
      for (int i = readPos; i < this.elements.length; i++) {
        this.elements[i] = elements[iterationCounter++];
      }

      //writing to bottom
      for(int i = 0; i < writePos; i++){
        this.elements[i] = elements[iterationCounter++];
      }
    }
  }

  public List<T> asList() {
    T[] elements = toArray();
    return Arrays.asList(elements);
  }

  public T[] toArray() {
    return !flipped
        ? Arrays.copyOfRange(elements, readPos, writePos)
        : concatenate(
            Arrays.copyOfRange(elements, readPos, capacity),
            Arrays.copyOfRange(elements, 0, writePos)
        );
  }

  public Object[] toObjectArray() {
    return toArray();
  }

  public Boolean isEmpty() {
    return getSize() == 0;
  }

  public void put(T element) {
    if (!flipped) {
      if (writePos == capacity) {
        writePos = 0;
        flipped = true;

        if (writePos < readPos) {
          elements[writePos++] = element;
        } else {
          throw new RuntimeException(); // not the best option but it is what the task says
        }
      } else {
        elements[writePos++] = element;
      }
    } else {
      if (writePos < readPos) {
        elements[writePos++] = element;
      } else {
        throw new RuntimeException();
      }
    }
  }

  public T get() {
    if (!flipped) {
      if (readPos < writePos) {
        return elements[readPos++];
      } else {
        throw new RuntimeException();
      }
    } else {
      if (readPos == capacity) {
        readPos = 0;
        flipped = false;

        if (readPos < writePos) {
          return elements[readPos++];
        } else {
          throw new RuntimeException();
        }
      } else {
        return elements[readPos++];
      }
    }
  }

  @SuppressWarnings("unchecked")
  public void addAll(List<? extends T> toAdd) {
    int newElementsLength = toAdd.size();

    if ( newElementsLength > elements.length - getSize()) {
      throw new RuntimeException(); // какую лучше ошибку использовать для этого?
    }

    T[] newElements = (T[]) toAdd.toArray();

    int newElementsReadPos = 0;
    if(!flipped){
      // readPos lower than writePos - free sections are:
      // from 0 to readPos
      // from writePos to capacity

      if(newElementsLength <= capacity - writePos){
        // new elements fit into top of elements array

        for(; newElementsReadPos < newElementsLength; newElementsReadPos++){
          elements[writePos++] = newElements[newElementsReadPos];
        }
      } else {
        //new elements must be divided between top and bottom of elements array

        //writing to top
        for(; writePos < capacity; writePos++){
          elements[writePos] = newElements[newElementsReadPos++];
        }

        //writing to bottom
        writePos = 0;
        flipped  = true;
        int endPos = Math.min(readPos, newElementsLength - newElementsReadPos);

        for(; writePos < endPos; writePos++){
          this.elements[writePos] = newElements[newElementsReadPos++];
        }
      }

    } else {
      // readPos higher than writePos - free sections are:
      // from writePos to readPos

      int endPos = Math.min(readPos, writePos + newElementsLength);

      for(; writePos < endPos; writePos++){
        elements[writePos] = newElements[newElementsReadPos++];
      }
    }
  }

  private Integer getSize() {
    return !flipped
        ? writePos - readPos
        : capacity - readPos + writePos;
  }

  private T[] concatenate(T[] a, T[] b) {
    int aLen = a.length;
    int bLen = b.length;

    @SuppressWarnings("unchecked")
    T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
    System.arraycopy(a, 0, c, 0, aLen);
    System.arraycopy(b, 0, c, aLen, bLen);

    return c;
  }
}